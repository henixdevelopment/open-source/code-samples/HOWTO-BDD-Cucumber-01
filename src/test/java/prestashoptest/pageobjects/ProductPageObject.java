package prestashoptest.pageobjects;

import java.util.regex.Pattern;

import prestashoptest.seleniumtools.PageObjectBase;

/**
 * Product page
 *
 */
public class ProductPageObject extends PageObjectBase {

    public ProductPageObject() {
        super(Pattern.compile("\\w+/\\d+-(\\d+-)?[-\\w]+\\.html"));
    }

    /**
     * Add one instance of the product to the cart
     */
    public void addToCart() {
        clickElement(SelectBy.CLASS, "add-to-cart");

        // wait for the popup to be displayed
        waitElementIsVisible(SelectBy.ID, "myModalLabel");

        //  close the popup by clicking on the backdrop
        clickElement(SelectBy.ID, "blockcart-modal");
    }
}
